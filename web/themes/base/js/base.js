/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {


  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.slides = {
    attach: function (context, settings) {
      $(context).find('.field-name-field-slides.swiper-container').once('ifLatestSlider').each(function (){
        var slides = new Swiper ('.field-name-field-slides.swiper-container', {
          autoplay: true,
          loop: true,
          effect: 'fade',
          pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true,
          },
        });
      });

      $(context).find('.uppa .field-name-field-room-images').once('ifSlider').each(function (){
        var slides = new Swiper (this, {
          autoplay: true,
          loop: false,
          slidesPerView:'1',
          effect: 'slide',
          pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
          }
        });
      });
      $(context).find('.reveal.large').once('ifRoomSlider').each(function () {

        $(this).on("open.zf.reveal", function(e){
          $(this).find('.swiper-container').each(function () {
            var slidesRev = new Swiper(this, {
              autoplay: true,
              loop: false,
              slidesPerView: '1',
              effect: 'slide',
              pagination: {
                el: $(this).find('.swiper-pagination'),
                type: 'bullets',
                clickable: true
              }
            });
          });
        });
        $(this).on("closed.zf.reveal", function(e){
          $(this).find('.swiper-container').each(function () {

          });
        });
      });

    }
  };

  Drupal.behaviors.popUp = {
    attach: function (context, settings) {
      $(context).find('#block-popup').once('ifPopUp').each(function () {

        $('#block-popup').foundation('open');


        $('#close-pop-up').on('click', function () {
          $('#block-popup').foundation('close');
        });
      });
    }
  };

  Drupal.behaviors.imagesLoaded = {
    attach: function (context, settings) {
      $('.grid').imagesLoaded(function () {
        $('.grid').masonry({
          itemSelector: '.grid-item',
          fitWidth: true,
          gutter: 30
        });
      });
      $('#lightgal').lightGallery();



        // Get the text from the url if there is one.
        var address = $('.field-name-field-straatnaam').text().trim();
        var title = $('h2 .field-wrapper').text().trim();
        var amount = $('.image-budget .field-name-field-available-rooms .field-item').text().trim();
        var amount2 = $('.street-wrapper .field-name-field-available-rooms .field-item').text().trim();
        var adtitle = $('.block-base-content > article > header > figure > figcaption > h2 a span.field-wrapper').text().trim();
        var academy = $('.field-name-field-academy .field-item').text().trim().substring(13);
        var adroom = $('.block-base-content > article > header > figure > .street-wrapper .field-name-field-straatnaam .field-item').text().trim();
      $('.form-item-room input').each(function() {
        var room = $(this).parent().parent().parent().parent().parent().parent().parent().attr("room-number");
        $(this).val(room);
      });

      $('.node--type-building span.inner-right-address').text(address);
      $('.node--type-building .inneramount').text(amount);
      $('.node--type-roomtype .inneramount').text(amount2);
      $('.node--type-roomtype span.inner-right-address').text(adroom);

      $('.building.teaser .field-name-field-available-rooms .field-item').each(function() {
        var xddd = $(this).text().trim();
        if(xddd.indexOf('Volzet') >= 0){
          $(this).parent().parent().parent().parent().parent().addClass('fully-booked');
        }
        if(xddd.indexOf('fully') >= 0){
          $(this).parent().parent().parent().parent().parent().addClass('fully-booked-eng');
        }
        if(xddd.indexOf('complet') >= 0){
          $(this).parent().parent().parent().parent().parent().addClass('fully-booked-fr');
        }

      });

      var numcheck = $('.roomtype.full figcaption [class^=field-name-field-number] .field-item').text().trim();

      $('.roomtype-teaser [class^=field-name-field-number] .field-item').each(function() {
        if(numcheck === $(this).text().trim()){
         $(this).parent().parent().parent().parent().parent().addClass('same-room');
        }

      });

      $('.node--type-building .form-item-year input').val(academy);
      $('.node--type-roomtype .form-item-year input').val(academy);
        if (address) {
          $('.node--type-building .form-item-building input').val(title);
          $('.node--type-roomtype .form-item-building input').val(adtitle);
        }

      var buildemail = $('.field-name-field-straatnaam .field-item').text().trim();
      var slicer = buildemail.slice(buildemail.length - 4);
      if(slicer.indexOf('rijk') >= 0){
        $('.node--type-building input[data-drupal-selector="edit-emailbuild"]').val('kortrijk@diggitstudentlife.eu');
        $('.node--type-roomtype input[data-drupal-selector="edit-emailbuild"]').val('kortrijk@diggitstudentlife.eu');
      }else{
        if(slicer.indexOf('trai') >= 0){
          $('.node--type-building input[data-drupal-selector="edit-emailbuild"]').val('kortrijk@diggitstudentlife.eu');
          $('.node--type-roomtype input[data-drupal-selector="edit-emailbuild"]').val('kortrijk@diggitstudentlife.eu');
        }else{
          if(slicer.indexOf('ssel') >= 0){
            $('.node--type-building input[data-drupal-selector="edit-emailbuild"]').val('brussel@diggitstudentlife.eu');
            $('.node--type-roomtype input[data-drupal-selector="edit-emailbuild"]').val('brussel@diggitstudentlife.eu');
          }else {
            if (slicer.indexOf('sels') >= 0) {
              $('.node--type-building input[data-drupal-selector="edit-emailbuild"]').val('brussel@diggitstudentlife.eu');
              $('.node--type-roomtype input[data-drupal-selector="edit-emailbuild"]').val('brussel@diggitstudentlife.eu');
            } else {
              if (slicer.indexOf('lles') >= 0) {
                $('.node--type-building input[data-drupal-selector="edit-emailbuild"]').val('brussel@diggitstudentlife.eu');
                $('.node--type-roomtype input[data-drupal-selector="edit-emailbuild"]').val('brussel@diggitstudentlife.eu');
              }else {
                $('.node--type-building input[data-drupal-selector="edit-emailbuild"]').val('hello@diggitstudentlife.eu');
                $('.node--type-roomtype input[data-drupal-selector="edit-emailbuild"]').val('hello@diggitstudentlife.eu');
              }
            }
          }
        }
      }
    }
  };




  /**
   * Use this behavior as a template for custom Javascript.
   */

  Drupal.behaviors.exampleBehavior = {
    attach: function (context, settings) {
      if ( document.documentElement.lang.toLowerCase() === "nl" ) {
        var $loca = $('.room-wrapper select#room').val();
        var $budg =$('.budgetz-wrapper select#budget').val();
        var $fullurl = "/nl/search-room?Location=" + $loca + "&Budget=" + $budg;
        var $nobudg = "/nl/search-room?Location=" + $loca;
        $('a.send-search').attr('href', $fullurl);
        $('.path-frontpage a.send-search').attr('href', $nobudg);
        $( "select#room" ).change(function() {
          $loca = $('.room-wrapper select#room').val();
          $budg =$('.budgetz-wrapper select#budget').val();
          $fullurl = "/nl/search-room?Location=" + $loca + "&Budget=" + $budg;
          var $nobudg = "/nl/search-room?Location=" + $loca;
          $('a.send-search').attr('href', $fullurl);
          $('.path-frontpage a.send-search').attr('href', $nobudg);
        });
        $( "select#budget" ).change(function() {
          $loca = $('.room-wrapper select#room').val();
          $budg =$('.budgetz-wrapper select#budget').val();
          $fullurl = "/nl/search-room?Location=" + $loca + "&Budget=" + $budg;
          var $nobudg = "/nl/search-room?Location=" + $loca;
          $('a.send-search').attr('href', $fullurl);
          $('.path-frontpage a.send-search').attr('href', $nobudg);
        });

        var $amres = $('div.views-element-container>div div.views-row').length;
        document.getElementById("amres").innerHTML = $amres;
      }
      if ( document.documentElement.lang.toLowerCase() === "en" ) {
        var $loca = $('.room-wrapper select#room').val();
        var $budg =$('.budgetz-wrapper select#budget').val();
        var $fullurl = "/en/search-room?Location=" + $loca + "&Budget=" + $budg;
        var $nobudg = "/en/search-room?Location=" + $loca;
        $('a.send-search').attr('href', $fullurl);
        $('.path-frontpage a.send-search').attr('href', $nobudg);
        $( "select#room" ).change(function() {
          $loca = $('.room-wrapper select#room').val();
          $budg =$('.budgetz-wrapper select#budget').val();
          $fullurl = "/en/search-room?Location=" + $loca + "&Budget=" + $budg;
          var $nobudg = "/en/search-room?Location=" + $loca;
          $('a.send-search').attr('href', $fullurl);
          $('.path-frontpage a.send-search').attr('href', $nobudg);
        });
        $( "select#budget" ).change(function() {
          $loca = $('.room-wrapper select#room').val();
          $budg =$('.budgetz-wrapper select#budget').val();
          $fullurl = "/en/search-room?Location=" + $loca + "&Budget=" + $budg;
          var $nobudg = "/en/search-room?Location=" + $loca;
          $('a.send-search').attr('href', $fullurl);
          $('.path-frontpage a.send-search').attr('href', $nobudg);
        });

        var $amres = $('div.views-element-container>div div.views-row').length;
        document.getElementById("amres").innerHTML = $amres;

      }
      if ( document.documentElement.lang.toLowerCase() === "fr" ) {
        var $loca = $('.room-wrapper select#room').val();
        var $budg =$('.budgetz-wrapper select#budget').val();
        var $fullurl = "/fr/search-room?Location=" + $loca + "&Budget=" + $budg;
        var $nobudg = "/fr/search-room?Location=" + $loca;
        $('a.send-search').attr('href', $fullurl);
        $('.path-frontpage a.send-search').attr('href', $nobudg);
        $( "select#room" ).change(function() {
          $loca = $('.room-wrapper select#room').val();
          $budg =$('.budgetz-wrapper select#budget').val();
          $fullurl = "/fr/search-room?Location=" + $loca + "&Budget=" + $budg;
          var $nobudg = "/fr/search-room?Location=" + $loca;
          $('a.send-search').attr('href', $fullurl);
          $('.path-frontpage a.send-search').attr('href', $nobudg);
        });
        $( "select#budget" ).change(function() {
          $loca = $('.room-wrapper select#room').val();
          $budg =$('.budgetz-wrapper select#budget').val();
          $fullurl = "/fr/search-room?Location=" + $loca + "&Budget=" + $budg;
          var $nobudg = "/fr/search-room?Location=" + $loca;
          $('a.send-search').attr('href', $fullurl);
          $('.path-frontpage a.send-search').attr('href', $nobudg);
        });

        var $amres = $('div.views-element-container>div div.views-row').length;
        document.getElementById("amres").innerHTML = $amres;

      }
    }
  };



          /**
           * Use this behavior as a template for custom Javascript.
           */
  Drupal.behaviors.cookieSettings = {
    attach: function (context, settings) {
      $('span[data-action="cookie_settings"]', context).click(function() {
        $('.eu-cookie-withdraw-tab').click();
      });


    }
  };



    /**
     * offCanvas behaviors
     */
    Drupal.behaviors.offCanvas = {
        attach: function (context, settings) {
            $(context).find('#offCanvasRightOverlap').once('ifRightCanvas').each(function () {
                $menuButton = $('#menuButton');
                $(this).on("opened.zf.offcanvas", function(e){
                    $menuButton.addClass('is-active');
                });
                $(this).on("closed.zf.offcanvas", function(e){
                    $menuButton.removeClass('is-active');
                });
            });
        }
    };

  Drupal.behaviors.langSwitch = {
    attach: function (context, settings) {
      //alert("I'm alive!");
      $("#block-languageswitcher li.nl a").text("NL");
      $("#block-languageswitcher li.fr a").text("FR");
      $("#block-languageswitcher li.en a").text("EN");


    }
  };

  Drupal.behaviors.kotKompas = {
    attach: function (context, settings) {
     $('.kotkompas-reveal form input.webform-button--submit').click(function(){
         $(this).parent().parent().hide();
       });

    }
  };




})(jQuery, Drupal);
