<?php

namespace Drupal\mvi_teamleader\Controller;

use Drupal\Core\Controller\ControllerBase;
use Nascom\TeamleaderApiClient\Repository\ContactRepository;

class TeamleaderTestController extends ControllerBase
{
  public function test() {

    $teamleader_service = \Drupal::service('teamleader_api');
    /** @var \Nascom\TeamleaderApiClient\Teamleader $teamleader_client */
    $teamleader_client = $teamleader_service->getClient();

    /** @var ContactRepository $contactrepo */
    $contactrepo = $teamleader_client->contacts();

    $contacts = $contactrepo->listContacts();

    $xd = $contacts;

    $build = [
      '#markup' => $this->t('Test'),
    ];
    return $build;
  }
}
