<?php


namespace Drupal\mvi_teamleader\Plugin\WebformHandler;

use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Nascom\TeamleaderApiClient\Model\Aggregate\Email;
use Nascom\TeamleaderApiClient\Model\Aggregate\Lead;
use Nascom\TeamleaderApiClient\Model\Aggregate\LinkedCustomer;
use Nascom\TeamleaderApiClient\Model\Aggregate\LinkedCustomField;
use Nascom\TeamleaderApiClient\Model\Aggregate\LinkedDefinition;
use Nascom\TeamleaderApiClient\Model\Aggregate\Telephone;
use Nascom\TeamleaderApiClient\Model\Contact\Contact;
use Nascom\TeamleaderApiClient\Model\Deal\Deal;
use Nascom\TeamleaderApiClient\Repository\ContactRepository;
use Nascom\TeamleaderApiClient\Request\CRM\Contacts\ContactsInfoRequest;

/**
 * Posts submission data to TL
 *
 * @WebformHandler(
 * id = "room_lead_webform_handler",
 * label = @Translation("Room lead webform handler"),
 * category = @Translation("Room"),
 * description = @Translation("Posts submission data to TL"),
 * cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 * results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class RoomLeadWebformHandler extends WebformHandlerBase
{
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE)
  {
    //Assigning submission data to variables

    $data_building = $webform_submission->getElementData('building');
    $data_year = $webform_submission->getElementData('year');
    $data_room = $webform_submission->getElementData('room');
    $data_first_name = $webform_submission->getElementData('first_name');
    $data_email = $webform_submission->getElementData('email');
    $data_last_name = $webform_submission->getElementData('last_name');
    $data_phone = $webform_submission->getElementData('phone');
    $data_country = $webform_submission->getElementData('country');
    $data_school = $webform_submission->getElementData('school');
    $data = $webform_submission->getData();

    //Connecting with teamleader

    $teamleader_service = \Drupal::service('teamleader_api');
    /** @var \Nascom\TeamleaderApiClient\Teamleader $teamleader_client */
    $teamleader_client = $teamleader_service->getClient();
    $teamleaderApiClient = $teamleader_service->getApiClient();

    //Get data of custom fields


    $customfields = $teamleader_client->customFieldDefinition();
    $xdz = $customfields->listCustomFieldDefinitions([
      'size' => 20,
      'number' => 3
    ]);


    //Add contact to teamleader
    $linkedContact = null;
    if ($data_last_name != '') {
      /** @var ContactRepository $contactrepo */
      $contactrepo = $teamleader_client->contacts();
      $contact = new Contact();
      $contact->setLastName($data_last_name);
      $customfieldsc = [];

      if($data_first_name != '') {
        $contact->setFirstName($data_first_name);
      }

      if($data_email != '') {
        $email = new Email($data_email, 'primary');
        $contact->setEmails([$email]);
      }

      if($data_phone != '') {
        $telephone = new Telephone();
        $telephone->setNumber($data_phone);
        $telephone->setType('phone');
        $contact->setTelephones([$telephone]);
      }

      // If data_country is not empty, then push that data to custom field array
      if($data_country != ''){
        // 78488845-be52-064d-9153-8c7e9fa46291 is uid from custom field in TL
      }

      // If custom field array is not empty, add it to the contact

      if (!empty($customfields)){
        $contact->setCustomFields($customfieldsc);
      }

      $linkedContact = $contactrepo->addContact($contact);

    }

    // Update contact with custom fields
    if (isset($linkedContact)) {
      // If data_school is not empty, then push that data to custom field array
      if (($data_school != '') || ($data_country != '')) {
        $addedContact = $teamleader_client->contacts()->getContact($linkedContact->getId());
        $customFields = $addedContact->getCustomFields();
        foreach ($customFields as $key => $customField) {
          $customFieldId = $customField->getDefinition()->getId();
          // 253ef371-2180-015a-9c55-613043646295 is '4. school' id
          if (($customFieldId === '253ef371-2180-015a-9c55-613043646295') && ($data_school != '')) {
            $customFields[$key]->setValue($data_school);
          }
          // 78488845-be52-064d-9153-8c7e9fa46291 is 2. Place of Birth id
          if (($customFieldId === '78488845-be52-064d-9153-8c7e9fa46291') && ($data_country != '')) {
            $customFields[$key]->setValue($data_country);
          }
        }
        $addedContact->setCustomFields($customFields);
        $contactrepo->updateContact($addedContact);
      }

      if($data_school != ''){
        // 253ef371-2180-015a-9c55-613043646295 is uid from custom field in TL

      }
    }

    //Add deal to teamleader

    if(isset($linkedContact)){
      $dealrepo = $teamleader_client->deals();
      $customer = new LinkedCustomer();
      $customer->setType('contact');
      $customer->setId($linkedContact->getId());
      $lead = new Lead();
      $lead->setCustomer($customer);
      $deal = new Deal();
      $deal->setLead($lead);
      $title = 'Kamer' . ': ' . $data_last_name;
      $deal->setTitle($title);
      $customfields = [];

      // If data_year is not empty, then push that data to custom field array

      if($data_year != ''){
        $linkeddefinition = new LinkedDefinition();
        // c4fd8363-fc03-0048-8456-9b1f221685e3 is uid from custom field in TL
        $linkeddefinition->setId('c4fd8363-fc03-0048-8456-9b1f221685e3');
        $customfield = new LinkedCustomField();
        $customfield->setDefinition($linkeddefinition);
        $customfield->setValue($data_year);
        $customfields[] = $customfield;
      }


      // If data_room is not empty, then push that data to custom field array

      if($data_room != ''){
        // b7f7ca62-df00-01b5-9955-996d367685e5 is uid from custom field in TL
        $linkeddefinition = new LinkedDefinition();
        $linkeddefinition->setId('b7f7ca62-df00-01b5-9955-996d367685e5');
        $customfield = new LinkedCustomField();
        $customfield->setDefinition($linkeddefinition);
        $customfield->setValue($data_room);
        $customfields[] = $customfield;
      }

      // If data_building is not empty, then push that data to custom field array

      if($data_building != ''){
        // bdab5fd76-d379-0582-ab59-e1f7874685e4 is uid from custom field in TL
        $linkeddefinition = new LinkedDefinition();
        $linkeddefinition->setId('dab5fd76-d379-0582-ab59-e1f7874685e4');
        $customfield = new LinkedCustomField();
        $customfield->setDefinition($linkeddefinition);
        $customfield->setValue($data_building);
        $customfields[] = $customfield;
      }
      // If custom field array is not empty, add it to the deal
      if (!empty($customfields)){
        $deal->setCustomFields($customfields);
      }
      $dealrepo->addDeal($deal);
    }


    parent::postSave($webform_submission, $update); // TODO: Change the autogenerated stub
  }
}
