<?php

namespace Drupal\mvi\Controller;

use Drupal\Core\Controller\ControllerBase;

class DashboardController extends ControllerBase {

    public function page() {
        $build = [
            '#markup' => null,
        ];

        return $build;
    }

}